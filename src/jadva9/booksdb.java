/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package jadva9;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;

/**
 *
 * @author Ho Hang
 */
public class booksdb {
    
    public Connection conn = null;
    public booksdb(){
        try{
            conn = DriverManager.getConnection("jdbc:mysql://localhost:3306/books?zeroDateTimeBehavior=convertToNull",
                    "root","password");            
        }
        catch(SQLException ex){
            
        }
    }
    
    public ResultSet getAuthors() throws SQLException{
        //get authors
        
        Statement stmt = conn.createStatement();
        String query = "select * from authors;";
        ResultSet rs = stmt.executeQuery(query);
        return rs;
    }
    
    public ResultSet getBooksByAuthor(String firstname, String lastname) throws SQLException{
        //get all authors and titles
        
        Statement stmt = conn.createStatement();
        String query = String.format("select * from titles inner join authorisbn"
               +" on titles.isbn = authorisbn.isbn"
               +" inner join authors on authorisbn.authorid"
               +"= authors.authorid where FirstName='%s' and LastName='%s' order by LastName, FirstName;",firstname,lastname);
        ResultSet rs = stmt.executeQuery(query);
        return rs;
    }
    
    public ResultSet getTitles() throws SQLException{
        //get all titles
        
        Statement stmt = conn.createStatement();
        String query = "select * from titles;";
        ResultSet rs = stmt.executeQuery(query);
        return rs;
    }
    
    public ResultSet insertAuthor(String firstname, String lastname) throws SQLException{
        //add author to database
        
        Statement stmt = conn.createStatement();
        String insertquery = String.format("insert into authors (FirstName, LastName) values ('%s', '%s')", firstname, lastname);
        stmt.executeUpdate(insertquery);
        String query = "select * from authors;";
        ResultSet rs = stmt.executeQuery(query);
        return rs;
    }
    
    public ResultSet insertBook(String firstname, String lastname, String edition, String copyright, String title, String isbn) throws SQLException{
        //add book and author
        Statement stmt = conn.createStatement();
        String insertauthorquery = String.format("insert into authors (FirstName, LastName) values ('%s', '%s')", firstname, lastname);
        stmt.executeUpdate(insertauthorquery);
        String lastidquery = String.format("select max(authorid) from authors");
        ResultSet lastid = stmt.executeQuery(lastidquery);
        String lid = "";
        while(lastid.next()){
            lid = lastid.getString(1);
        }
        String inserttitlequery = String.format("insert into titles values (%s, '%s', %s, '%s')",isbn, title, edition, copyright);
        stmt.executeUpdate(inserttitlequery);
        
        String insauthisbnquery = String.format("insert into authorisbn values (%s, %s)", lid, isbn);
        stmt.executeUpdate(insauthisbnquery);
        String authtitlesquery = String.format("select * from titles inner join authorisbn"
               +" on titles.isbn = authorisbn.isbn"
               +" inner join authors on authorisbn.authorid"
               +"= authors.authorid");
        ResultSet rs = stmt.executeQuery(authtitlesquery);
        return rs;
    }
    
    public ResultSet customSelect(String query) throws SQLException{
        Statement stmt = conn.createStatement();        
        ResultSet rs = null;        
        rs = stmt.executeQuery(query);
        return rs;
    }
    
    public void customUpdate(String query) throws SQLException{
        Statement stmt = conn.createStatement();        
        stmt.executeUpdate(query);        
    }
    
}
